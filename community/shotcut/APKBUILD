# Contributor: Carlo Landmeter <clandmeter@alpinelinux.org>
# Maintainer: Kevin Daudt <kdaudt@alpinelinux.org>
pkgname=shotcut
pkgver=23.09.29
pkgrel=1
pkgdesc="Cross-platform video editor"
url="https://www.shotcut.org"
# armhf, armv7: no opengl1.1 support, only 2
# aarch64: upstream does not support gcc
# riscv64: no opencv
arch="all !armhf !armv7 !aarch64 !riscv64"
license="GPL-3.0-or-later"
depends="ffmpeg qt5-qtquickcontrols"
makedepends="
	cmake
	fftw-dev
	mlt-dev
	qt6-qtbase-dev
	qt6-qtmultimedia-dev
	qt6-qttools-dev
	qt6-qtwebsockets-dev
	samurai
	"
subpackages="$pkgname-doc $pkgname-lang"
source="$pkgname-$pkgver.tar.xz::https://github.com/mltframework/shotcut/releases/download/v$pkgver/shotcut-src-${pkgver//./}.txz
	shotcut-23.09.29-dont-shadow-FINAL-properties.patch
	launcher
	"
builddir="$srcdir/src/shotcut"
options="!check" # No test suite present

build() {
	export CXXFLAGS="$CXXFLAGS -O2 -DNDEBUG -DSHOTCUT_NOUPGRADE"
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build
}

lang() {
	pkgdesc="Languages for package shotcut"
	install_if="$pkgname=$pkgver-r$pkgrel lang"
	amove usr/share/shotcut/translations
}

package() {
	DESTDIR="$pkgdir" cmake --install build
	mv "$pkgdir"/usr/bin/shotcut "$pkgdir"/usr/bin/shotcut-real
	install -Dm755 "$srcdir"/launcher "$pkgdir"/usr/bin/shotcut
}

sha512sums="
7ea0d6e7e2f2439b590c724f9ebf55cea8dd02f528ec340e847e2bf04afcb32ff3d930bc1c2f24a7dfbe4285bf104dcf652041fd2abefbd56ccd809ebda53bf1  shotcut-23.09.29.tar.xz
d24fabecd5d4fa4318854ed7d87efd86ed3898cbf662fcb1aba29063c0d5be6be468f951891ff7918e28c51584ae5992146292b835157a136741dad9a44b22ac  shotcut-23.09.29-dont-shadow-FINAL-properties.patch
c9d4263cf5c4a1964ad73fb810353e338e6417e3241a177f444ef151c2da7970eaaa0ca94cfcf52da4d3fe9b1abc4d5fac78361c287aa7e10e3aab2026893cca  launcher
"
