# Contributor: Henrik Riomar <henrik.riomar@gmail.com>
# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=swaylock-effects
pkgver=1.7.0.0_rc1
_pkgver=${pkgver/_/-}
pkgrel=0
pkgdesc="Screen locker for Wayland with fancy effects"
url="https://github.com/jirutka/swaylock-effects"
arch="all"
license="MIT"
options="!check" # no test suite
makedepends="
	cairo-dev
	libxkbcommon-dev
	linux-pam-dev
	meson
	ninja
	scdoc
	wayland-dev
	wayland-protocols
	"
provider_priority=10  # lowest (other provider is swaylock aport)
provides="swaylock"
subpackages="$pkgname-dbg $pkgname-doc"
source="https://github.com/jirutka/swaylock-effects/archive/v$_pkgver/$pkgname-$_pkgver.tar.gz"
builddir="$srcdir/$pkgname-$_pkgver"

prepare() {
	default_prepare

	sed -i 's/login/base-auth/g' pam/swaylock

	cat <<-__EOF__ >> pam/swaylock

	# Unlock GNOME Keyring if available
	-auth		optional	pam_gnome_keyring.so
	-session	optional	pam_gnome_keyring.so auto_start

	# Unlock KWallet if available
	-auth		optional	pam_kwallet.so
	-auth		optional	pam_kwallet5.so
	-session	optional	pam_kwallet.so auto_start
	-session	optional	pam_kwallet5.so auto_start
	__EOF__
}

build() {
	# NOTE: completions are the same as the original swaylock provides.
	abuild-meson \
		-Dgdk-pixbuf=disabled \
		-Dbash-completions=false \
		-Dfish-completions=false \
		-Dzsh-completions=false \
		. output
	meson compile -C output --verbose
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

dbg() {
	depends="!swaylock-dbg"

	default_dbg
}

doc() {
	provider_priority=10  # lowest (other provider is swaylock aport)
	provides="swaylock-doc"

	default_doc
}

sha512sums="
f352aca2ad1daaea5d3d6c1a9a8cf2b7ede3361b0b528c8345a4f57e7d793eb0b7f9c316bb4c49e06a120bd54ed5dc29d2006cec74f6e76211154d43e0a462a3  swaylock-effects-1.7.0.0-rc1.tar.gz
"
